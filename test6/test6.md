<!-- markdownlint-disable MD033-->

<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

这里是一个基于Oracle数据库的商品销售系统的数据库设计方案:

## 表空间设计:

创建两个表空间:

- USERS01:存放用户和权限相关的数据,会占用较少空间,使用大块空间管理方式。  
- PRODUCTS:存放商品及销售相关的数据,数据量较大,使用自动空间管理方式。

```sql
CREATE TABLESPACE USERS01
DATAFILE 'users01.dbf' 
SIZE 100M 
EXTENT MANAGEMENT LOCAL 
SEGMENT SPACE MANAGEMENT MANUAL
ONLINE;
```

```sql
CREATE TABLESPACE PRODUCTS01 
DATAFILE 'products01.dbf' 
SIZE 500M
AUTOEXTEND ON NEXT 30M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL 
SEGMENT SPACE MANAGEMENT AUTO
ONLINE;
```

## 表设计:

- 用户表(USERS):存放用户信息。约10万条数据。  
  
  ```sql
  CREATE TABLE USERS(
     USER_ID NUMBER(8) PRIMARY KEY,
     USERNAME VARCHAR2(20) NOT NULL UNIQUE, 
     PASSWORD VARCHAR2(20) NOT NULL,
     ENABLED CHAR(1) DEFAULT 'Y' 
  ) 
  TABLESPACE USERS01;
  ```

- 商品表(PRODUCTS):存放商品信息。约50万条数据。  
  
  ```sql
  CREATE TABLE PRODUCTS(
     PROD_ID NUMBER(8) PRIMARY KEY, 
     PROD_NAME VARCHAR2(50) NOT NULL UNIQUE,
     CATEGORY VARCHAR2(20), 
     UNIT_PRICE NUMBER(8,2),
     STATUS CHAR(1) DEFAULT 'Y'  
  )
  TABLESPACE PRODUCTS01;
  ```

- 订单表(ORDERS):存放订单信息。约40万条数据。  
  
  ```sql
  CREATE TABLE ORDERS(
     ORDER_ID NUMBER(8) PRIMARY KEY,
     USER_ID NUMBER(8) NOT NULL,
     CREATE_TIME TIMESTAMP NOT NULL,
     AMOUNT NUMBER(8,2) NOT NULL,
     STATUS VARCHAR2(20) NOT NULL 
  )
  TABLESPACE PRODUCTS01; 
  ```

- 订单详情表(ORDER_DETAILS):存放订单详情信息。约200万条数据。
  
  ```sql
  CREATE TABLE ORDER_DETAILS(
     DETAIL_ID NUMBER(8) PRIMARY KEY,
     ORDER_ID NUMBER(8) NOT NULL, 
     PROD_ID NUMBER(8) NOT NULL,
     BUY_NUM NUMBER(8) NOT NULL,
     AMOUNT NUMBER(8,2) NOT NULL 
  )  
  TABLESPACE PRODUCTS01;
  ```
  
  

插入模拟数据

```sql
DECLARE
   USER_ID NUMBER(8);
BEGIN
   FOR i IN 1..20000 LOOP
      USER_ID := i;
      INSERT INTO USERS (USER_ID, USERNAME, PASSWORD) 
      VALUES (USER_ID, 'USER' || USER_ID, 'PWD' || USER_ID);
   END LOOP;
END;  
/
```

```sql
DECLARE
   PROD_ID NUMBER(8);
BEGIN
   FOR i IN 1..100000 LOOP
      PROD_ID := i;
      INSERT INTO PRODUCTS (PROD_ID, PROD_NAME, CATEGORY, UNIT_PRICE)
      VALUES (PROD_ID, 'PROD' || PROD_ID, 'CAT' || MOD(PROD_ID,10), 
             TRUNC(DBMS_RANDOM.VALUE(5, 200))); 
   END LOOP;
END; 
/
```

```sql
DECLARE 
   ORDER_ID NUMBER(8);
   USER_ID NUMBER(8);
   AMOUNT NUMBER(8,2);
BEGIN
   FOR i IN 1..200000 LOOP
      USER_ID := TRUNC(DBMS_RANDOM.VALUE(1, 20000));
      AMOUNT := TRUNC(DBMS_RANDOM.VALUE(50, 5000));
      INSERT INTO ORDERS (ORDER_ID, USER_ID, CREATE_TIME, AMOUNT, STATUS)
      VALUES (i, USER_ID, SYSDATE, AMOUNT, 'OPEN');   --指定STATUS值为OPEN
   END LOOP;
END;
/
```

```sql

DECLARE
   DETAIL_ID NUMBER(8);   
   ORDER_ID NUMBER(8);
   PROD_ID NUMBER(8);
   BUY_NUM NUMBER(8);
   AMOUNT NUMBER(8,2);  
   PRICE NUMBER(8,2);   
BEGIN    
   FOR i IN 1..1000000 LOOP    
      ORDER_ID := TRUNC(DBMS_RANDOM.VALUE(1, 200000));     
      PROD_ID := TRUNC(DBMS_RANDOM.VALUE(1, 100000));     
      BUY_NUM := TRUNC(DBMS_RANDOM.VALUE(1, 10));  

     SELECT UNIT_PRICE INTO PRICE 
     FROM "PRODUCTS"
     WHERE PROD_ID = PROD_ID 
     AND ROWNUM = 1;  

     AMOUNT := BUY_NUM * PRICE;

     INSERT INTO ORDER_DETAILS (DETAIL_ID, ORDER_ID, PROD_ID, BUY_NUM,   AMOUNT)  
     VALUES (i, ORDER_ID, PROD_ID, BUY_NUM, AMOUNT);  
   END LOOP;   
END;
/
```

## 权限及用户分配:

- 创建用户sales_admin,密码为123456,拥有所有权限。  

- 创建用户sales_clerk,密码为123456,只有数据查询和新增权限。
  
  ```sql
  CREATE USER sales_admin IDENTIFIED BY 123456;
  GRANT DBA TO sales_admin;  --授予所有权限
  
  CREATE USER sales_clerk IDENTIFIED BY 123456;
  GRANT SELECT, INSERT ON "PRODUCTS" TO sales_clerk;  
  GRANT SELECT, INSERT ON ORDERS TO sales_clerk;
  ```
  
  

## 程序包设计:

创建程序包PRODUCT_SALES,包含:  

```sql
CREATE OR REPLACE PACKAGE PRODUCT_SALES AS
  PROCEDURE SALES_ANALYSIS(YEAR NUMBER);  
  FUNCTION GET_SALES(PROD_ID NUMBER) RETURN NUMBER;
END PRODUCT_SALES;
```

* 设计存储过程计算订单金额、修改商品价格、生成订单编号

* 设计函数计算每个客户的订单总金额、每个商品的销售总额
  
  ```sql
  CREATE OR REPLACE PACKAGE PACK_SALE AS
    -- 存储过程：计算订单金额
    PROCEDURE calculate_order_amount(p_order_id IN NUMBER);
  
    -- 存储过程：修改商品价格
    PROCEDURE update_product_price(p_product_id IN NUMBER, p_new_price IN NUMBER);
  
    -- 存储过程：生成订单编号
    PROCEDURE generate_order_id(p_customer_id IN NUMBER);
  
    -- 函数：计算每个客户的订单总金额
    FUNCTION calculate_total_order_amount(p_customer_id IN NUMBER) RETURN NUMBER;
  
    -- 函数：计算每个商品的销售总额
    FUNCTION calculate_total_sales_amount(p_product_id IN NUMBER) RETURN NUMBER;
  END PACK_SALE;
  /
  
  CREATE OR REPLACE PACKAGE BODY PACK_SALE AS
    -- 存储过程：计算订单金额
    PROCEDURE calculate_order_amount(p_order_id IN NUMBER) AS
      v_order_amount NUMBER;
    BEGIN
      -- 在这里编写计算订单金额的逻辑，将结果存储到 v_order_amount 变量中
  
      -- 示例逻辑：假设订单金额为订单详情表中数量（quantity）乘以单价（price）的总和
      SELECT SUM(quantity * price) INTO v_order_amount
      FROM order_items
      WHERE order_id = p_order_id;
  
      -- 输出订单金额
      DBMS_OUTPUT.PUT_LINE('Order Amount: ' || v_order_amount);
    END calculate_order_amount;
  
    -- 存储过程：修改商品价格
    PROCEDURE update_product_price(p_product_id IN NUMBER, p_new_price IN NUMBER) AS
    BEGIN
      -- 在这里编写修改商品价格的逻辑
  
      -- 示例逻辑：更新商品表中的价格字段
      UPDATE products
      SET price = p_new_price
      WHERE product_id = p_product_id;
    END update_product_price;
  
    -- 存储过程：生成订单编号
    PROCEDURE generate_order_id(p_customer_id IN NUMBER) AS
      v_order_id NUMBER;
    BEGIN
      -- 在这里编写生成订单编号的逻辑，将结果存储到 v_order_id 变量中
  
      -- 示例逻辑：假设订单编号为自增长序列的下一个值
      SELECT order_id_seq.NEXTVAL INTO v_order_id
      FROM dual;
  
      -- 插入新的订单记录
      INSERT INTO orders(order_id, customer_id, order_date)
      VALUES (v_order_id, p_customer_id, SYSDATE);
  
      -- 输出生成的订单编号
      DBMS_OUTPUT.PUT_LINE('Generated Order ID: ' || v_order_id);
    END generate_order_id;
  
    -- 函数：计算每个客户的订单总金额
    FUNCTION calculate_total_order_amount(p_customer_id IN NUMBER) RETURN NUMBER AS
      v_total_amount NUMBER;
    BEGIN
      -- 在这里编写计算每个客户的订单总金额的逻辑，将结果存储到 v_total_amount 变量中
  
      -- 示例逻辑：计算订单详情表中某客户的订单金额总和
      SELECT SUM(quantity * price) INTO v_total_amount
      FROM order_items oi
      INNER JOIN orders o ON oi.order_id = o.order_id
      WHERE o.customer_id = p_customer_id;
  
      -- 返回订单总金额
      RETURN v_total_amount;
    END calculate_total_order_amount;
  
    -- 函数：计算每个商品的销售总额
    FUNCTION calculate_total_sales_amount(p_product_id IN NUMBER) RETURN NUMBER AS
      v_total_amount NUMBER;
    BEGIN
      -- 在这里编写计算每个商品的销售总额的逻辑，将结果存储到 v_total_amount 变量中
  
      -- 示例逻辑：计算订单详情表中某商品的销售总额
      SELECT SUM(quantity * price) INTO v_total_amount
      FROM order_items
      WHERE product_id = p_product_id;
  
      -- 返回销售总额
      RETURN v_total_amount;
    END calculate_total_sales_amount;
  END PACK_SALE;
  /
  ```
  
  

## 备份方案:

每天晚上进行数据库的热备份,对changed block进行备份。将备份文件传输到异地存储,一周备份一次全备份,全备份文件也存储到异地。本地保留最近一周的备份,异地保留三个月的备份。  
每月进行一次冷备份,对整个数据库进行备份,并将备份文件异地保存12个月。

```shell
# backup_script.sh (Linux/Unix)

# 生成时间戳
timestamp=$(date +%Y%m%d%H%M%S)

# 导出数据库
expdp username/password DIRECTORY=data_pump_dir DUMPFILE=database_backup_${timestamp}.dmp FULL=YES

# 压缩备份文件
gzip database_backup_${timestamp}.dmp

# 将压缩后的备份文件拷贝到异地DR环境（假设DR环境挂载在 /mnt/dr 目录下）
cp database_backup_${timestamp}.dmp.gz /mnt/dr/
```

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - test6_design.docx学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项      | 评分标准               | 满分  |
|:-------- |:------------------ |:--- |
| 文档整体     | 文档内容详实、规范，美观大方     | 10  |
| 表设计      | 表设计及表空间设计合理，样例数据合理 | 20  |
| 用户管理     | 权限及用户分配方案设计正确      | 20  |
| PL/SQL设计 | 存储过程和函数设计正确        | 30  |
| 备份方案     | 备份方案设计正确           | 20  |
