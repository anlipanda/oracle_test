# backup_script.sh (Linux/Unix)

# 导出数据库
expdp username/password DIRECTORY=data_pump_dir DUMPFILE=database_backup.dmp FULL=YES

# 将备份文件拷贝到磁带（假设磁带挂载在 /mnt/tape 目录下）
cp database_backup.dmp /mnt/tape/
