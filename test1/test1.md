# 实验1：SQL语句的执行计划分析与优化指导
姓名：潘炳青 学号：202010414114

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 参考

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

- 查询所有部门的部门总人数和最高工资。

查询1：

```SQL

$sqlplus hr/123@localhost/pdborcl

set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
max(e.salary)as "最高工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
GROUP BY d.department_name;

输出结果：
DEPARTMENT_NAME                部门总人数   最高工资
------------------------------ ---------- ----------
Administration				1	4400
Accounting				2      12008
Purchasing				6      11000
Human Resources 			1	6500
IT					5	9000
Public Relations			1      10000
Executive				3      24000
Shipping			       45	8200
Sales				       34      14000
Finance 				6      12008
Marketing				2      13000

执行计划
----------------------------------------------------------
Plan hash value: 1139150879

--------------------------------------------------------------------------------
-------------

| Id  | Operation		      | Name	    | Rows  | Bytes | Cost (%CPU
)| Time     |

--------------------------------------------------------------------------------
-------------

|   0 | SELECT STATEMENT	      | 	    |	 27 |	621 |	  7  (29
)| 00:00:01 |

|   1 |  HASH GROUP BY		      | 	    |	 27 |	621 |	  7  (29
)| 00:00:01 |

|   2 |   MERGE JOIN		      | 	    |	106 |  2438 |	  6  (17
)| 00:00:01 |

|   3 |    TABLE ACCESS BY INDEX ROWID| DEPARTMENTS |	 27 |	432 |	  2   (0
)| 00:00:01 |

|   4 |     INDEX FULL SCAN	      | DEPT_ID_PK  |	 27 |	    |	  1   (0
)| 00:00:01 |

|*  5 |    SORT JOIN		      | 	    |	107 |	749 |	  4  (25
)| 00:00:01 |

|   6 |     TABLE ACCESS FULL	      | EMPLOYEES   |	107 |	749 |	  3   (0
)| 00:00:01 |

--------------------------------------------------------------------------------
-------------


Predicate Information (identified by operation id):
---------------------------------------------------

   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

统计信息
----------------------------------------------------------
	 22  recursive calls
	  0  db block gets
	 15  consistent gets
	  0  physical reads
	  0  redo size
	991  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	 11  rows processed

```

#### 查询运行用时
![查询测试图片](cx001.png)

- 查询2

### sqldeveloper的优化指导工具优化结果
![查询1测试图片](cx002.png)

## 实验注意事项

- 完成时间：2023-3-21，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test1目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
