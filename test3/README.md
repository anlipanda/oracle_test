## 实验名称



创建分区表 

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。 

## 实验内容

本实验使用实验 2 的 sale 用户创建两张表：订单表(orders)与订单详表(order_details)。
两个表通过列 order_id 建立主外键关联。给表 orders.customer_name 增加 B_Tree 索引。
新建两个序列，分别设置 orders.order_id 和 order_details.id，插入数据的时候，不需要手工设置这两个 ID 值。
orders 表按订单日期（order_date）设置范围分区。
order_details 表设置引用分区。
表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders 表的数据都大于 40 万行，order_details 表的数据大于 200 万行（每个订单对应 5 个 order_details）。
写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
进行分区与不分区的对比实验 

## 实验步骤

### sql-developer 软件创建表，使用脚本创建表单与分区

![本地路径](001.png "相对路径演示")

### 给表orders.customer_name增加B_Tree索引。)

![](002.png)



### 创建 order_details 表

![本地路径](003.png "相对路径演示")

### 创建序列 SEQ1 的语句

![本地路径](004.png "相对路径演示")

### 插入 100 条 orders 记录的样例脚本

![本地路径](005.png "相对路径演示")
